<?php

namespace Drupal\commerce_tax_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'commerce_tax_rate_empty_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "commerce_tax_rate_empty_formatter",
 *   label = @Translation("Commerce tax rate empty formatter"),
 *   field_types = {
 *     "commerce_tax_rate"
 *   }
 * )
 */
class CommerceTaxRateEmptyFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // Does not actually output anything.
    return [];
  }


}
