<?php

namespace Drupal\commerce_tax_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'commerce_tax_rate_selector' widget.
 *
 * @FieldWidget(
 *   id = "commerce_tax_rate_selector",
 *   label = @Translation("Commerce tax rate selector"),
 *   field_types = {
 *     "commerce_tax_rate"
 *   }
 * )
 */
class CommerceTaxRateSelector extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'size' => 60,
      'placeholder' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['value'] = $element + [
        '#type'          => 'select',
        '#empty_option'  => t('- Select a tax rate - '),
        '#options'       => ['Rate #1', 'Rate #2'],
        '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : NULL,
    ];

    return $element;
  }

}
